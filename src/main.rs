/// Client
struct Client {
    connection: Connection,
}

impl Client {
    pub fn new(_connection: Connection) -> Self {
        Self {
            connection: _connection,
        }
    }

    pub fn print(&self) {
        self.connection.print();
    }
}

/// Connection
struct Connection {
    id: String,
}

impl Connection {
    pub fn new(_id: &str) -> Self {
        Self {
            id: String::from(_id),
        }
    }

    pub fn print(&self) {
        println!("id: {}", self.id);
    }
}

/// Room
struct Room {
    clients: Vec<Rc<RefCell<Client>>>,
}

impl Room {
    pub fn new() -> Self {
        Self {
            clients: Vec::new(),
        }
    }

    pub fn add_client(&mut self, client: &mut Client) {
        self.clients.push(client);
    }
}

fn main() {
    let conn = Connection::new("Hello");
    let client = Client::new(conn);

    client.print();

    let mut room = Room::new();

    room.add_client(client);

    client.print();
}
